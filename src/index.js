// because React is exported as default, we don't need to put it in {}
import React from 'react';
import ReactDOM  from 'react-dom';
import YTSearch from 'youtube-api-search';
import _ from 'lodash';

// we do not have to add the .js extension (it assumes it is a .js)
import SearchBar from './components/search_bar'
import VideoList from './components/video_list'
import VideoDetail from './components/video_detail.js'

const API_KEY = 'AIzaSyDo_RMIZh-UOM6JFlpeUDKDZb-vEXW8k3M';

// create a new component (just produce some html)
// take this component's generated HTML and put it on the page
// (in the DOM)

// JSX
// any JSX will be transpiled by React/Babel to become vanilla JS
// React.createElement('div', null, 'Hi!');
// this is a component class, we need an instance of a component
// instance: <App></App>, </App> if nothing is inbetween
class App extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      videos: [],
      selectedVideo: null
    };

    this.videoSearch('surfboards');

  }

  // search term
  videoSearch(term) {
    YTSearch({key: API_KEY, term}, (videos) => {
      this.setState({
        videos,
        selectedVideo: videos[0]
      });
    });
  }

  render() {
    // _.debounce throttles the function so can only be called once every 300 ms
    const videoSearch = _.debounce((term) => {
      this.videoSearch(term)
    }, 400);

    // since VideoList want to access the App's state for the list of videos,
    // react allows us to pass in data from parent to children
    // we do this by passing props
    // this will give an error because we are trying to fetch the youtube data/API
    // but it will still try to render! There is nothing there yet (asynchronous)
    // solution is in video_detail.js (we added a check that video is defined)
    return (
      <div>
        <SearchBar onSearchTermChange={videoSearch}/>
        <VideoDetail video={this.state.selectedVideo}/>
        <VideoList
          onVideoSelect={selectedVideo => this.setState({selectedVideo})}
          videos={this.state.videos}/>
      </div>
    );
  }
}

ReactDOM.render(<App/>, document.querySelector('.container'));
